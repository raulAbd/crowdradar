$( document ).ready(function() {
    fetchLocations();
});

function deleteLocation(id) {
	$.post("http://158.177.16.195:3001/delete_location", JSON.stringify({"id": id}), function(data) {
	});
	$("#" + id).hide()
};

function approveLocation(id) {
	console.log(id);
	$.post("http://158.177.16.195:3001/approve_location", JSON.stringify({"id": id}), function(data) {
	});
	$("#" + id).hide()
};

function refreshLocations() {
	$("#ai-passed").empty();
	$("#ai-not-passed").empty();
	fetchLocations();
};

function fetchLocations() {
	$.get( nodeAddress + "/get_unverified_locations", function( data ) {
		for (var i = 0; i < data["locations"].length; i++) {
			var location = document.createElement("div");
			location.id = data["locations"][i]["id"];
			var image = document.createElement("img");
			image.src = data["locations"][i]["photo"];
			image.setAttribute("class", "img-fluid");
			image.setAttribute("style", "height: 20vh; min-height: 200px");
			location.appendChild(image);
			var mapImage = document.createElement("img");
			mapImage.src = "https://image.maps.ls.hereapi.com/mia/1.6/mapview?apiKey=FyEIHp2c1W0FIkqEwPaDFbdDKmgzcRX9eYlEbCI_XK8&c=" + data["locations"][i]["coordinates"]["lat"] + "," + data["locations"][i]["coordinates"]["lon"] + "&u=" + data["locations"][i]["radius"] + "&z=15";
			mapImage.setAttribute("class", "img-fluid");
			mapImage.setAttribute("style", "height: 20vh; min-height: 200px");
			location.appendChild(mapImage);
			var category = document.createElement("div");
			category.textContent = "Description: " +  data["locations"][i]["category"];
			var timestamp = document.createElement("div");
			timestamp.textContent = "timestamp: " +  data["locations"][i]["datetime"];
			location.appendChild(category);
			location.appendChild(timestamp);
			var buttonsWrapper = document.createElement("div");
			buttonsWrapper.className = "flex-container";
			var approveButton = document.createElement("button");
			approveButton.textContent = "Approve"
			approveButton.setAttribute("onClick", "approveLocation('" + data["locations"][i]["id"] + "');");
			approveButton.setAttribute("class", "btn btn-success")
			var deleteButton = document.createElement("button");
			deleteButton.textContent = "Delete";
			deleteButton.setAttribute("onClick", "deleteLocation('" + data["locations"][i]["id"] + "');");
			deleteButton.setAttribute("class", "btn btn-danger")
			buttonsWrapper.appendChild(approveButton);
			buttonsWrapper.appendChild(deleteButton);
			location.setAttribute("style", "margin: 10px")
			location.appendChild(buttonsWrapper);

			if (data["locations"][i]["ai_passed"]) {
				document.getElementById("ai-passed").appendChild(location);
			} else {
				document.getElementById("ai-not-passed").appendChild(location);
			}
		}
	});
};
