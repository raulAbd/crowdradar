var locationsOnPage = new Set();

function getImage(image, id) {
	$.post(nodeAddress + "/get_photos", JSON.stringify({"ids": [id]}), function(data) {
		console.log(data);
		image.src = data["location_photos"][0]["photo"];
		image.setAttribute("style", "height: 20vh; min-height: 200px");
	})
};

function setCenterToLocation(lat, lon) {
	map.setCenter({lat: lat, lng: lon});
}

function fetchLocations() {
	$.get( pythonAddress + "/authority_dashboard/feed", function( data ) {
		console.log(data);
		for (var i = 0; i < data["locations"].length; i++) {
			if (!locationsOnPage.has(data["locations"][i]["id"])) {
				locationsOnPage.add(data["locations"][i]["id"]);

				console.log(data["locations"][i]["coordinates"]);
				var marker = new H.map.Marker({lat: data["locations"][i]["coordinates"]["lat"],
					lng: data["locations"][i]["coordinates"]["lon"]});
    			map.addObject(marker);

				var location = document.createElement("div");
				location.id = data["locations"][i]["id"];
				var image = document.createElement("img");
				getImage(image, data["locations"][i]["id"]);
				image.setAttribute("onclick", "setCenterToLocation(" + data["locations"][i]["coordinates"]["lat"] + ", " + data["locations"][i]["coordinates"]["lon"] + ");");
				location.appendChild(image);
				var category = document.createElement("div");
				category.textContent = "Description: " +  data["locations"][i]["category"];
				var timestamp = document.createElement("div");
				timestamp.textContent = "Timestamp: " +  data["locations"][i]["datetime"];
				location.appendChild(category);
				location.setAttribute("style", "margin: 20px")
				location.appendChild(timestamp);

				document.getElementById("locations").prepend(location);
			}
		}
	});
}


setInterval(function() {
	fetchLocations();
}, 500);

var platform = new H.service.Platform({
  apikey: "FyEIHp2c1W0FIkqEwPaDFbdDKmgzcRX9eYlEbCI_XK8"
});
var defaultLayers = platform.createDefaultLayers();


function setMapViewBounds(map){
	$.get(pythonAddress + "/authority_dashboard/get_bbox", function(data) {

		var bbox = new H.geo.Rect(42.3736,-71.0751,42.3472,-71.0408);
		var bbox = new H.geo.Rect(data["bounding_box"]["coordinates_top_right"]["lat"],
			data["bounding_box"]["coordinates_bottom_left"]["lon"],
			data["bounding_box"]["coordinates_bottom_left"]["lat"],
			data["bounding_box"]["coordinates_top_right"]["lon"] );
		map.getViewModel().setLookAtData({
	    	bounds: bbox
		});
	})
}

var map = new H.Map(document.getElementById('map'),
  defaultLayers.vector.normal.map,{
  center: {lat:50, lng:5},
  zoom: 4,
  pixelRatio: window.devicePixelRatio || 1
});

window.addEventListener('resize', () => map.getViewPort().resize());

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

var ui = H.ui.UI.createDefault(map, defaultLayers);
setMapViewBounds(map);
