function displayBounds(firstMap) {
    var viewModel = firstMap.getViewModel();

    firstMap.addEventListener('mapviewchange', function() {
        var data = JSON.stringify(viewModel.getLookAtData().bounds.getBoundingBox().toGeoJSON());
        $('#bounding_box').val(data);
    });
}

var platform = new H.service.Platform({
  apikey: "FyEIHp2c1W0FIkqEwPaDFbdDKmgzcRX9eYlEbCI_XK8"
});

var defaultLayers = platform.createDefaultLayers();
var defaultLayersSync = platform.createDefaultLayers();

var mapContainer = document.createElement('div');

mapContainer.style.height = '500px';
mapContainer.style.width = '500px';

document.getElementById('map').appendChild(mapContainer);

var map = new H.Map(mapContainer,
  defaultLayers.vector.normal.map,{
    center: {lat: 52.5206970, lng: 13.40927320},
    zoom: 16,
    pixelRatio: window.devicePixelRatio || 1
});

window.addEventListener('resize', () => map.getViewPort().resize());

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

var ui = H.ui.UI.createDefault(map, defaultLayers);

displayBounds(map);
$("#map").hide();

function checkIfShowMap() {
  if ($("#select_group").val() == "authority") {
    navigator.geolocation.getCurrentPosition(function(location) {
        map.setCenter({lat: location.coords.latitude, lng: location.coords.longitude});
    });
    $("#map").show();
  } else {
    $("#map").hide();
  }
}
                  