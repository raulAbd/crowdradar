from flask import Flask, jsonify, abort, request, make_response, render_template, redirect, url_for, \
    send_file
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
import logging
from flask_login import LoginManager, UserMixin, login_user, login_required, current_user, logout_user
from flask_sqlalchemy import SQLAlchemy
import passlib
from passlib.context import LazyCryptContext
from sqlalchemy_utils import PasswordType
from os import path
from pprint import pprint
import keys
import requests
import json

app = Flask(__name__, static_url_path="")
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + path.join(path.abspath(path.dirname(__file__)), 'users.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = keys.LOGIN_MANAGER_KEY
login_manager = LoginManager()
login_manager.init_app(app)
auth = HTTPBasicAuth()
db = SQLAlchemy(app)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), unique=True)
    password = db.Column(
        PasswordType(
            schemes=[
                'pbkdf2_sha512',
            ],
        ),
    )
    group = db.Column(db.String(30), unique=False)
    bounding_box = db.Column(db.String(350), unique=False)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('login', next=request.endpoint))


@app.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('login_form'))


@app.route('/login', methods=['GET'])
def login_form():
    return render_template('login.html', next=request.args.get('next'))


@app.route('/login', methods=['POST'])
def login():
    user = User.query.filter_by(username=request.form['username']).first()
    if user is not None and user.password == request.form['password']:
        login_user(user, remember=True)

    if request.form['next'] != "None":
        return redirect(url_for(request.form['next']))
    else:
        return redirect(url_for('index'))


@auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username=username).first()
    if user is not None:
        return user.password == password
    return False


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/')
@login_required
def index():
    if current_user.group == "admin":
        return redirect(url_for('admin_dashboard'))
    elif current_user.group == "moderator":
        return redirect(url_for('moderator_dashboard'))
    elif current_user.group == "authority":
        return redirect(url_for('authority_dashboard'))
    else:
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.route('/admin_dashboard', methods=['GET'])
@login_required
def admin_dashboard():
    if current_user.group != "admin":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        admins = User.query.filter_by(group="admin")
        admins = [admin.username for admin in admins]
        authorities = User.query.filter_by(group="authority")
        authorities = [authority.username for authority in authorities]
        moderators = User.query.filter_by(group="moderator")
        moderators = [moderator.username for moderator in moderators]
        return render_template("admin.html", admins=admins, authorities=authorities, moderators=moderators)


@app.route('/admin_dashboard', methods=['POST'])
@login_required
def manage_users():
    if current_user.group != "admin":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        user = User.query.filter_by(username=request.form['username']).first()
        if request.form['action'] == 'Create':
            if user is None:
                bb = ""
                if request.form['group'] == 'authority':
                    bb = json.loads(request.form['bounding_box'])["coordinates"]
                    bb = {
                        "bounding_box": {
                            "coordinates_top_right": {
                                "lat": bb[1][1],
                                "lon": bb[1][0]
                            },
                            "coordinates_bottom_left": {
                                "lat": bb[3][1],
                                "lon": bb[3][0]
                            }
                        }
                    }
                    bb = json.dumps(bb)
                new_user = User(username=request.form['username'], password=request.form['password'],
                                group=request.form['group'], bounding_box=bb)
                db.session.add(new_user)
                db.session.commit()

            else:
                return "Username already exists"
        else:
            return redirect(url_for('bad_request'))
        return redirect(request.url)


@app.route('/admin_dashboard/delete_account/<username>', methods=['GET'])
@login_required
def delete_account(username):
    if current_user.group != "admin":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        user = User.query.filter_by(username=username).first()
        if user is not None:
            db.session.delete(user)
            db.session.commit()
            return redirect(request.url)
        else:
            return redirect(url_for('admin_dashboard'))


@app.route('/authority_dashboard')
@login_required
def authority_dashboard():
    if current_user.group != "authority":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        return render_template("authority.html")


@app.route('/authority_dashboard/feed')
@login_required
def authority_dashboard_feed():
    if current_user.group != "authority":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        locations = requests.post("http://158.177.16.195:3001/get_locations",
                                  json=json.loads(current_user.bounding_box)).json()
        pprint(locations)
        return jsonify(locations), 200


@app.route('/authority_dashboard/get_bbox')
@login_required
def authority_dashboard_get_bbox():
    if current_user.group != "authority":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        return jsonify(json.loads(current_user.bounding_box)), 200


@app.route('/moderator_dashboard')
@login_required
def moderator_dashboard():
    if current_user.group != "moderator":
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)
    else:
        return render_template("moderator.html")


@app.route('/get_map')
def get_map():
    return render_template("gmap.html")


def start(host, port):
    app.run(debug=True, host=host, port=port)


if __name__ == '__main__':
    start("0.0.0.0", 5000)
