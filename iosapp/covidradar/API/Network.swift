//
//  Network.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Moya
import Moya_ObjectMapper
import RxSwift

protocol Networking {
    var provider: MoyaProvider<CovidRadar> { get }
    func request(target: CovidRadar) -> PrimitiveSequence<SingleTrait, Response>
}

class Network: Networking {
    
    static let shared = Network()
    
    let provider: MoyaProvider<CovidRadar>
    
    init() {
        
        let endpointClosure = { (target: CovidRadar) -> Endpoint in
            
            let endpoint: Endpoint = Endpoint(url: URL(target: target).absoluteString, sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: target.method, task: target.task, httpHeaderFields: target.headers)
            
            return endpoint
            
        }
        
        provider = MoyaProvider<CovidRadar>(endpointClosure: endpointClosure,
                                        plugins: (Helper.shared.debug)
                                            ?
                                                [
                                                    NetworkLoggerPlugin()
                                                ]
                                            :
                                            [])
    }
    
    func request(target: CovidRadar) -> PrimitiveSequence<SingleTrait, Response> {
        //print("REQUEST", target)
        return provider.rx
            .request(target)
            .filterSuccessfulStatusCodes()
    }
}
