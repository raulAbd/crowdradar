//
//  CovidRadar.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import Moya
import RxSwift

public enum CovidRadar {
    case requestLocations(r: GetLocationsRequest)
    case uploadLocation(image: Data, r: UploadLocationRequest)
    case getRoute(r: GetRouteRequest)
}

extension CovidRadar: TargetType {
    
    public var baseURL: URL {
        let apiURL = "http://158.177.16.195:3001"
        return URL(string: apiURL)!
    }
    
    public var path: String {
        switch self {
        case .requestLocations:
            return "/get_locations"
        case .uploadLocation:
            return "/new_location"
        case .getRoute:
            return "/get_safe_route"
        }
    }
    
    public var method: Moya.Method {
        return .post
    }
    
    public var sampleData: Data {
        return stubbedResponse("test")
    }
    
    public var task: Task {
        switch self {
        case let .requestLocations(r):
            return .requestJSONEncodable(r)
        case let .uploadLocation(data, desc):
            
            let descriptionData = try! JSONEncoder().encode(desc)
            
            let imgData = MultipartFormData(provider: .data(data), name: "file", fileName: "image.jpg", mimeType: "image/jpeg")
            
            let descMultipartData = MultipartFormData(provider: .data(descriptionData), name: "description")
            
            let multipartData = [imgData, descMultipartData]

            return .uploadMultipart(multipartData)
        case let .getRoute(request):
            return .requestJSONEncodable(request)
        default:
            return .requestPlain
        }
    }
    
    public var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}



// MARK: - Provider support

func stubbedResponse(_ filename: String, type: String? = nil) -> Data! {
    @objc class TestClass: NSObject { }
    
    let bundle = Bundle(for: TestClass.self)
    let path = bundle.path(forResource: filename, ofType: type ?? "json")
    return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
}

