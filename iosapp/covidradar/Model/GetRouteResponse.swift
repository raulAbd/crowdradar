//
//  GetRouteResponse.swift
//  covidradar
//
//  Created by Alex Mavrichev on 19.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import ObjectMapper

struct GetRouteResponse: Mappable {
    
    var points: [RoutePoint] = []
    var google_url: String?
    
    init() {
        
    }
    
    // MARK: JSON
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        points <- map["points"]
        google_url <- map["google_url"]
    }
}
