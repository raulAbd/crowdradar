//
//  GetLocationsRequest.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation

public struct GetLocationsRequest: Encodable {
    let bounding_box: BoundingBox
}

public struct BoundingBox: Encodable {
    let coordinates_top_right: BoundingBoxPoint
    let coordinates_bottom_left: BoundingBoxPoint
}

public struct BoundingBoxPoint: Encodable {
    let lat: Double
    let lon: Double
}
