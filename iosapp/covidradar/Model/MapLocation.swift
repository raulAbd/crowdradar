//
//  MapLocation.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import ObjectMapper

struct MapLocation: Mappable {
    
    var id = ""
    var datetime = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
    var category: String?
    var radius: Double = 0.0
    
    // MARK: JSON
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        lat <- map["coordinates.lat"]
        lng <- map["coordinates.lon"]
        category <- map["category"]
        radius <- map["radius"]
        datetime <- map["datetime"]
        id <- map["id"]
    }
}
