//
//  GetRouteRequest.swift
//  covidradar
//
//  Created by Alex Mavrichev on 19.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation

public struct GetRouteRequest: Encodable {
    let from: GetRouteRequestPoint
    let to: GetRouteRequestPoint
}

public struct GetRouteRequestPoint: Encodable {
    let coordinates: BoundingBoxPoint
}
