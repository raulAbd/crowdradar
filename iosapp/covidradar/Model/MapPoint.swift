//
//  MapPoint.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import ObjectMapper

struct MapPoint: Mappable {
    
    var coordinates: [Double] = []
    
    // MARK: JSON
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        coordinates <- map["coordinates"]
    }
}
