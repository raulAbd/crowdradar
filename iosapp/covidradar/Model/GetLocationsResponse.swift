//
//  RequestLocations.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import ObjectMapper

struct GetLocationsResponse: Mappable {
    
    var locations: [MapLocation] = []
    
    init() {
        
    }
    
    // MARK: JSON
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        locations <- map["locations"]
    }
}
