//
//  UploadLocationRequest.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation

public struct UploadLocationRequest: Encodable {
    let coordinates: BoundingBoxPoint
    let category: String
}
