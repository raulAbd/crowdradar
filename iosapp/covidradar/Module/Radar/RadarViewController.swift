//
//  RadarViewController.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RMessage
import heresdk
import CoreLocation
import Presentr

class RadarViewController: UIViewController {
    
    enum State {
        case idle
        case capturePhoto
        case capturePhotoCategory
        case choosePhotoLocation
        case chooseRouteDestination
        case viewRoute
    }
    
    var state: State = .idle {
        didSet {
            updateUI()
        }
    }
    
    let rControl = RMController()
    
    let viewModel = RadarViewModel()
    let disposeBag = DisposeBag()
    
    let locationManager = CLLocationManager()
    var myLocation: CLLocation?
    
    var points = [CGPoint]()
    
    var circles = [MapCircleLite]()
    var markers = [MapMarkerLite]()
    var mapLocations = [MapLocation]()
    
    var myLocationCircle: MapCircleLite?
    
    
    var googleLink: String?
    
    var capturedPhoto: UIImage?
    var routePolyline: MapPolylineLite?
    var routeFinishMarker: MapMarkerLite?
    
    var cameraInitialPositionSet = false
    
    weak var pickerController: UIImagePickerController?
    
    lazy var mapView2: MapViewLite = {
        let m = MapViewLite(frame: view.bounds)
        
        // Load the map scene using a map style to render the map with.
        m.mapScene.loadScene(mapStyle: .normalDay, callback: onLoadScene)
        m.camera.addObserver(self)
        m.gestures.tapDelegate = self
        
        return m
    }()
    
    lazy var myPrositionButton: RoundedButton = {
        let b = RoundedButton()
        b.setImage(UIImage(named: "select"), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.imageEdgeInsets = UIEdgeInsets()
        
        return b
    }()
    
    lazy var crowdButton: RoundedButton = {
        let b = RoundedButton()
        b.setTitle("Capture crowd", for: .normal)
        b.setImage(UIImage(named: "photograph"), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        
        return b
    }()
    
    lazy var qnButton: RoundedButton = {
        let b = RoundedButton()
        //b.setTitle("Route", for: .normal)
        b.setImage(UIImage(named: "route"), for: .normal)
        b.imageEdgeInsets = UIEdgeInsets()
        b.translatesAutoresizingMaskIntoConstraints = false
        
        return b
    }()
    
    lazy var doneButton: RoundedButton = {
        let b = RoundedButton()
        b.setTitle("Done", for: .normal)
        b.setImage(UIImage(named: "check-mark"), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        
        return b
    }()
    
    lazy var googleButton: RoundedButton = {
        let b = RoundedButton()
        b.setTitle("Open in Maps", for: .normal)
        b.setImage(UIImage(named: "map-pin-location"), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        
        return b
    }()
    
    lazy var pinView: UIImageView = {
        let b = UIImageView(image: UIImage(named: "pin"))
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
    lazy var pulseView: UIView = {
        let view:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.layer.cornerRadius = 25
        view.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
        
        self.view.addSubview(view)
        
        let scaleAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        
        scaleAnimation.duration = 1.5
        scaleAnimation.repeatCount = .infinity
        scaleAnimation.autoreverses = false
        scaleAnimation.fromValue = 1
        scaleAnimation.toValue = 4
        
        view.layer.add(scaleAnimation, forKey: "scale")
        
        let opAnimation:CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        
        opAnimation.duration = 1.5
        opAnimation.repeatCount = .infinity
        opAnimation.autoreverses = false
        opAnimation.fromValue = 1
        opAnimation.toValue = 0
        
        view.layer.add(opAnimation, forKey: "opacity")
        
        return view
    }()
    
    lazy var titleView: TitleView = {
        let l = TitleView()
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let v = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.hidesWhenStopped = true
        v.backgroundColor = .white
        v.layer.cornerRadius = 20
        v.layer.masksToBounds = true
        return v
    }()
    
    lazy var photoPreview: UIImageView = {
        let l = UIImageView()
        //l.translatesAutoresizingMaskIntoConstraints = false
        l.frame = view.bounds
        l.isHidden = true
        l.contentMode = .scaleAspectFill
        l.clipsToBounds = true
        return l
    }()
    
    lazy var photoFormView: PhotoFormView = {
        let vs = Bundle.main.loadNibNamed("PhotoFormView", owner: self, options: nil)
        let v = vs!.first as! PhotoFormView
        v.translatesAutoresizingMaskIntoConstraints = false
        v.isHidden = false
        v.delegate = self
        //v.layer.cornerRadius = 25
        //v.clipsToBounds = true
        
        return v
    }()
    
    let presenter = Presentr(presentationType: .popup)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        view.addSubview(mapView2)
        view.addSubview(pulseView)
        view.addSubview(myPrositionButton)
        view.addSubview(crowdButton)
        view.addSubview(qnButton)
        view.addSubview(titleView)
        view.addSubview(doneButton)
        view.addSubview(pinView)
        view.addSubview(activityIndicator)
        view.addSubview(googleButton)
        view.addSubview(photoPreview)
        view.addSubview(photoFormView)
        
        setupConstraints()
        setupBindings()
        updateUI()
//        createHeatMapLayer()
    }
    
    func placeMarkers(_ newMarkers: [MapLocation]) {
        
        mapLocations = newMarkers
        
        var _circles = [MapCircleLite]()
        var _markers = [MapMarkerLite]()
        //print("place markers \(newMarkers.count)")
        
        for m in newMarkers {
            let geoAt = GeoCoordinates(latitude: m.lat, longitude: m.lng)
            
            let geoCircle = GeoCircle(center: geoAt,
                                      //radiusInMeters: 200.0)
                                      radiusInMeters: m.radius)
            let mapCircleStyle = MapCircleStyleLite()
            mapCircleStyle.setFillColor(0x00908AA0, encoding: .rgba8888)
            let mapCircle = MapCircleLite(geometry: geoCircle, style: mapCircleStyle)
            
            _circles.append(mapCircle)
            mapView2.mapScene.addMapCircle(mapCircle)
            //print(geoCircle)
            
            let mapMarker = MapMarkerLite(at: geoAt)
            let image = UIImage(named: "poi")
            let mapImage = MapImageLite(image!)

            let mapMarkerImageStyle = MapMarkerImageStyleLite()
            mapMarkerImageStyle.setAnchorPoint(Anchor2D(horizontal: 0.5, vertical: 1))

            mapMarker.addImage(mapImage!, style: mapMarkerImageStyle)
            
            let metadata = Metadata()
            metadata.setString(key: "key_poi", value: m.id)
            mapMarker.metadata = metadata
            
            mapView2.mapScene.addMapMarker(mapMarker)
            
            _markers.append(mapMarker)
            
        }
        
//        let toRemove = markers
//            .filter { !circles.contains($0) }
//
//        let toAdd = circles
//            .filter { !markers.contains($0) }
//
//        print("added \(toAdd.count)")
//        print("removed \(toRemove.count)")
//
//        // remove old markers
//        toRemove.forEach { mapView2.mapScene.removeMapCircle($0) }
//        markers.removeAll { toRemove.contains($0) }
//
//        // add new markers
//        toAdd.forEach { mapView2.mapScene.addMapCircle($0) }
//        markers.append(contentsOf: toAdd)
        
        circles.forEach { mapView2.mapScene.removeMapCircle($0) }
        circles.removeAll(keepingCapacity: false)
        
        circles = _circles
        
        markers.forEach { mapView2.mapScene.removeMapMarker($0) }
        markers.removeAll(keepingCapacity: false)
        
        circles = _circles
        markers = _markers
        
    }
    
    func doneAction() {
        
        print("Done action")
        switch state {
        case .chooseRouteDestination:
            //self.state = .idle
            startRoating()
            break
        case .choosePhotoLocation:
            startPhotoUpload()
            break
        case .viewRoute:
            state = .idle
            if let route = routePolyline {
                mapView2.mapScene.removeMapPolyline(route)
                routePolyline = nil
            }
            if let rfm = routeFinishMarker {
                mapView2.mapScene.removeMapMarker(rfm)
                routeFinishMarker = nil
            }
            break
        default:
            break
        }
    }
    
    func getCenterCoordinate() -> GeoCoordinates {
        return mapView2.camera.viewToGeoCoordinates(viewCoordinates: Point2D(x: Double(mapView2.center.x * UIScreen.main.scale), y: Double(mapView2.center.y * UIScreen.main.scale)))
    }
    
    func startPhotoUpload() {
        let destination = getCenterCoordinate()
        
        let d = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
        
        doneButton.isHidden = true
        titleView.titleLabel.text = "Uploading data ..."
        
        viewModel.markerPoint.onNext(d)
        viewModel.uploadPhoto.onNext(())
    }
    
    func startRoating() {
        guard let myLocation = myLocation else {
            let alert = UIAlertController(title: "Where are you?", message: "We need access to your geolocation to build a walking route.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            self.present(alert, animated: true)
            
            return
        }
        
        
        
        let destination = getCenterCoordinate()
        
        let d = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
        
        print("Compute route from \(myLocation.coordinate) to \(destination)")
        
        doneButton.isHidden = true
        titleView.titleLabel.text = "Computing route ..."
        
        viewModel.routeStartPoint.onNext(myLocation.coordinate)
        viewModel.routeEndPoint.onNext(d)
        viewModel.getRoute.onNext(())
        
    }
    
    func handleRouteRequest(_ r: GetRouteResponse) {
            rControl.showMessage(
              withSpec: normalSpec,
              title: "Route has been calculated",
              body: "On this path, you can reach the goal without crossing areas with a large number of people!"
            )
            
            print(r.google_url)
            
    //        if let su = r.google_url {
    //            googleLink = su.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    //        }
            
            googleLink = r.google_url
            
            state = .viewRoute
            
            let coords = r.points.map { s -> GeoCoordinates in
                return GeoCoordinates(latitude: s.lat, longitude: s.lng)
            }
        
        print(r.points, coords)

            drawPath(points: coords)
        }
    
    func drawPath(points: [GeoCoordinates]) {
        print("Draw polyline \(points.count)")
        guard points.count > 0 else {
            return
        }
        let geoPolyline = try! GeoPolyline(vertices: points)
        let mapPolylineStyle = MapPolylineStyleLite()
        mapPolylineStyle.setWidthInPixels(inPixels: 20)
        mapPolylineStyle.setColor(0x00908AA0, encoding: .rgba8888)
        let mapPolyline = MapPolylineLite(geometry: geoPolyline, style: mapPolylineStyle)
        routePolyline = mapPolyline
        mapView2.mapScene.addMapPolyline(mapPolyline)
        
        
        let mapMarker = MapMarkerLite(at: points.last!)
        let image = UIImage(named: "flag")
        let mapImage = MapImageLite(image!)

        let mapMarkerImageStyle = MapMarkerImageStyleLite()
        mapMarkerImageStyle.setAnchorPoint(Anchor2D(horizontal: 0.0, vertical: 1))

        mapMarker.addImage(mapImage!, style: mapMarkerImageStyle)
        mapView2.mapScene.addMapMarker(mapMarker)
        
        routeFinishMarker = mapMarker
        
        
    }
    
    func centerMapOnMyLocation() {
        guard let myLocation = myLocation else {
            return
        }
        
        mapView2.camera.setTarget(GeoCoordinates(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude))
        mapView2.camera.setZoomLevel(14)
        mapView2.camera.setTilt(degreesFromNadir: 0)
    }
    
    func capturePhoto() {
        self.state = .capturePhoto
        
        let vc = UIImagePickerController()
        #if targetEnvironment(simulator)
        vc.sourceType = .photoLibrary
        #else
        vc.sourceType = .camera
        #endif
        vc.allowsEditing = false
        vc.delegate = self
        present(vc, animated: true)
        
        pickerController = vc
        
    }
    
    func updateMyLocation() {
        
        guard let myLocation = myLocation else {
            return
        }
        
        if !cameraInitialPositionSet {
            centerMapOnMyLocation()
            cameraInitialPositionSet = true
        }
        
        let geoCircle = GeoCircle(center: GeoCoordinates(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude),
                                  radiusInMeters: max(myLocation.horizontalAccuracy, 30.0))
        let mapCircleStyle = MapCircleStyleLite()
        mapCircleStyle.setFillColor(0x0A56A07a, encoding: .rgba8888)
        mapCircleStyle.setStrokeColor(0xebf5ffb3, encoding: .rgba8888)
        mapCircleStyle.setStrokeWidthInPixels(inPixels: 4)
        let mapCircle = MapCircleLite(geometry: geoCircle, style: mapCircleStyle)
        
        if let old = myLocationCircle {
            mapView2.mapScene.removeMapCircle(old)
        }
        
        myLocationCircle = mapCircle
        
        mapView2.mapScene.addMapCircle(mapCircle)
        
    }
    
    deinit {
        self.locationManager.stopUpdatingLocation()
    }
    
}

extension RadarViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        myLocation = location
        updateMyLocation()
    }
}
