//
//  LocationInfoViewController.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit
import Kingfisher

class LocationInfoViewController: UIViewController {
    
    @IBOutlet weak var titleOutlet: UILabel!
    
    @IBOutlet weak var dateOutlet: UILabel!
    
    @IBOutlet weak var imageOutlet: UIImageView!
    
    var location: MapLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        imageOutlet.kf.indicatorType = .activity
        
        let urlString = String(format: "http://158.177.16.195:3001/get_photo?id=%@", location.id)
        
        print("Image: \(urlString)")
        
        if let url = URL(string: urlString) {
            imageOutlet.kf.setImage(with: url)
        }
        
        titleOutlet.text = location.category
        dateOutlet.text = location.datetime
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
