//
//  RadarViewController+HERE.swift
//  covidradar
//
//  Created by Alex Mavrichev on 25.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit
import heresdk

extension RadarViewController {
    func onLoadScene(errorCode: MapSceneLite.ErrorCode?) {
        if let error = errorCode {
            print("Error: Map scene not loaded, \(error)")
        } else {
            // Configure the map.
            mapView2.camera.setTarget(GeoCoordinates(latitude: 48.134550, longitude: 11.581998))
            mapView2.camera.setZoomLevel(14)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        mapView2.handleLowMemory()
    }
}

extension RadarViewController: CameraObserverLite, TapDelegate {
    func onCameraUpdated(_ cameraUpdate: CameraUpdateLite) {
        //let mapCenter = cameraUpdate.target
        //print("Current map center location: \(mapCenter). Current zoom level: \(cameraUpdate.zoomLevel)")
        
        requestUpdateMap()
        
    }
    
    func requestUpdateMap() {
        let top = mapView2.camera.viewToGeoCoordinates(viewCoordinates: Point2D(x: 0.0, y: 0.0))
        
        let bottom = mapView2.camera.viewToGeoCoordinates(viewCoordinates: Point2D(x: Double(mapView2.frame.size.width * UIScreen.main.scale), y: Double(mapView2.frame.size.height * UIScreen.main.scale)))
        
        let box_top = BoundingBoxPoint(lat: top.latitude, lon: top.longitude)
        let box_bottom = BoundingBoxPoint(lat: bottom.latitude, lon: bottom.longitude)
        let box = BoundingBox(coordinates_top_right: box_top, coordinates_bottom_left: box_bottom)
        
//        let jsonData = try! JSONEncoder().encode(box)
//        let jsonString = String(data: jsonData, encoding: .utf8)!
//        print(jsonString)
        
        viewModel.refreshMap.onNext(box)
    }
    
    func onTap(origin: Point2D) {
        print("Tap on \(origin)")
        mapView2.pickMapItems(at: origin, radius: 2, completion: onMapItemsPicked)
    }
    
    // Completion handler to receive picked map items.
    func onMapItemsPicked(pickedMapItems: PickMapItemsResultLite?) {
        guard let topmostMapMarker = pickedMapItems?.topmostMarker else {
            return
        }
        
        guard let keyID = topmostMapMarker.metadata?.getString(key: "key_poi") else {
            return
        }
        
        
        guard let idx = mapLocations.firstIndex (where: { (m) -> Bool in
            m.id == keyID
        }) else {
            return
        }
        
        guard let loc = mapLocations[safe: idx] else {
            return
        }

        print("Map marker picked:", "Location: \(topmostMapMarker.coordinates)")
        print(loc.category, loc.datetime)
        
        presenter.roundCorners = true
        presenter.cornerRadius = 18
        presenter.dismissOnSwipe = true
        presenter.dismissOnSwipeDirection = .top
        
        let controller = LocationInfoViewController(nibName: "LocationInfoViewController", bundle: Bundle.main)
        controller.location = loc
        
        customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
    }
}
