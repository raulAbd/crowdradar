//
//  RadarViewController+RxBindings.swift
//  covidradar
//
//  Created by Alex Mavrichev on 25.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
//import GoogleMaps
//import GoogleMapsUtils

extension RadarViewController {
    func setupBindings() {
        myPrositionButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.centerMapOnMyLocation()
            })
            .disposed(by: disposeBag)
        
        crowdButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.capturePhoto()
            })
            .disposed(by: disposeBag)
        
        qnButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.state = .chooseRouteDestination
            })
            .disposed(by: disposeBag)
        
        doneButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.doneAction()
            })
            .disposed(by: disposeBag)
        
        viewModel.locations
            .subscribe(onNext: { [unowned self] markers in
                self.placeMarkers(markers)
            })
            .disposed(by: disposeBag)
        
        viewModel.photoSent
            .subscribe(onNext: { [unowned self]  _ in
                print("Photo sent")
                self.state = .idle
                self.requestUpdateMap()
            })
            .disposed(by: disposeBag)
        
        googleButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                print("Go to google", self.googleLink)
                if let googleLink = self.googleLink {
                    //print(googleLink)
                    guard let url = URL(string: googleLink) else {
                      return //be safe
                    }
                    
                    //print(url)

                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.routeData
            .subscribe(onNext: { [unowned self] r in
                self.handleRouteRequest(r)
            })
            .disposed(by: disposeBag)
        
        viewModel.saving
            .bind(to: activityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        
        requestUpdateMap()
        
    }
}
