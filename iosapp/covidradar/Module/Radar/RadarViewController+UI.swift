//
//  RadarViewController+UI.swift
//  covidradar
//
//  Created by Alex Mavrichev on 25.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit

extension RadarViewController {
    func updateUI() {
        
        pinView.isHidden = !(state == .chooseRouteDestination || state == .choosePhotoLocation)
        
        crowdButton.isHidden = (state != .idle)
        qnButton.isHidden = (state != .idle)
        titleView.isHidden = !(state == .chooseRouteDestination || state == .choosePhotoLocation)
        myPrositionButton.isHidden = !(state == .idle || state == .chooseRouteDestination || state == .choosePhotoLocation)
        pulseView.isHidden = myPrositionButton.isHidden
        doneButton.isHidden = !(state == .choosePhotoLocation || state == .chooseRouteDestination || state == .viewRoute)
        googleButton.isHidden = true
        
        photoFormView.isHidden = !(state == .capturePhotoCategory)
        photoPreview.isHidden = !(state == .capturePhotoCategory || state == .choosePhotoLocation)
        
        switch state {
        case .idle:
            print("Idle state")
            break
        case .capturePhoto:
            print("Capture photo state")
            break
        case .capturePhotoCategory:
            photoPreview.frame = view.bounds
            print("Capture photo category state")
            break
        case .chooseRouteDestination:
            print("Choose destination state")
            titleView.titleLabel.text = "Select a destination by moving the map"
            break
        case .choosePhotoLocation:
            print("Choose mark state")
            titleView.titleLabel.text = "Place your mark where you saw this crowd"
            break
        case .viewRoute:
            print("View route")
            break
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pulseView.center = myPrositionButton.center
    }
    
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            myPrositionButton.widthAnchor.constraint(equalToConstant: 60),
            myPrositionButton.heightAnchor.constraint(equalToConstant: 60),
            myPrositionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            myPrositionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30)
        ])
        
        NSLayoutConstraint.activate([
            crowdButton.widthAnchor.constraint(equalToConstant: 175),
            crowdButton.heightAnchor.constraint(equalToConstant: 50),
            crowdButton.trailingAnchor.constraint(equalTo: myPrositionButton.leadingAnchor, constant: -41),
            crowdButton.centerYAnchor.constraint(equalTo: myPrositionButton.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            qnButton.widthAnchor.constraint(equalToConstant: 60),
            qnButton.heightAnchor.constraint(equalToConstant: 60),
            qnButton.centerXAnchor.constraint(equalTo: myPrositionButton.centerXAnchor),
            qnButton.bottomAnchor.constraint(equalTo: myPrositionButton.topAnchor, constant: -14)
        ])
        
        NSLayoutConstraint.activate([
            doneButton.widthAnchor.constraint(equalToConstant: 110),
            doneButton.heightAnchor.constraint(equalToConstant: 60),
            doneButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            doneButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -140)
        ])
        
        NSLayoutConstraint.activate([
            googleButton.widthAnchor.constraint(equalToConstant: 180),
            googleButton.heightAnchor.constraint(equalToConstant: 60),
            googleButton.trailingAnchor.constraint(equalTo: myPrositionButton.leadingAnchor, constant: -41),
            googleButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -140)
        ])
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            titleView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            titleView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            pinView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pinView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -23)
        ])
        
        NSLayoutConstraint.activate([
            activityIndicator.widthAnchor.constraint(equalToConstant: 40),
            activityIndicator.heightAnchor.constraint(equalToConstant: 40),
            activityIndicator.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            activityIndicator.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor)
        ])
        
        
        NSLayoutConstraint.activate([
            photoFormView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            photoFormView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -100),
            photoFormView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -100)
            //photoFormView.heightAnchor.constraint(equalToConstant: 60),
        ])
        
        
    }
}
