//
//  RadarViewModel.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

final class RadarViewModel {
    
    // MARK: - Input
    
    let refreshMap = PublishSubject<BoundingBox>()
    
    let uploadPhoto = PublishSubject<Void>()
    let category = PublishSubject<String>()
    let capturedPhoto = PublishSubject<Data>()
    let markerPoint = PublishSubject<CLLocationCoordinate2D>()
    
    let routeStartPoint = PublishSubject<CLLocationCoordinate2D>()
    let routeEndPoint = PublishSubject<CLLocationCoordinate2D>()
    
    let getRoute = PublishSubject<Void>()
    
    // MARK: - Output
    let locations: Observable<[MapLocation]>
    let routeData: Observable<GetRouteResponse>
    let photoSent: Observable<Void>
    var saving: Observable<Bool>
    
    init() {
        
        let saving = ActivityIndicator()
        self.saving = saving.asObservable()
        
        let locations = Observable.merge(refreshMap)
            .debounce(RxTimeInterval.milliseconds(1000), scheduler: MainScheduler.instance)
            .flatMap { box -> Observable<[MapLocation]> in
                
                let req = GetLocationsRequest(bounding_box: box)
                
                let jsonData = try! JSONEncoder().encode(req)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                print("Send json: \(jsonString)")
                
                return Network.shared
                    .request(target: .requestLocations(r: req))
                    .mapObject(GetLocationsResponse.self)
                    .asObservable()
                    .catchErrorJustReturn(GetLocationsResponse())
                    .flatMap({ (response) -> Observable<[MapLocation]> in
                        Observable.just(response.locations)
                    })
                    .trackActivity(saving)
        }
        
        let data = Observable.combineLatest(capturedPhoto, markerPoint, category) { (photo: $0, point: $1, cat: $2) }
        
        let photoSent = uploadPhoto
            .withLatestFrom(data)
            .flatMap { data -> Observable<Void> in
                
                let (photo, point, cat) = data
                
                let p = BoundingBoxPoint(lat: point.latitude, lon: point.longitude)
                let req = UploadLocationRequest(coordinates: p, category: cat)
                
                let jsonData = try! JSONEncoder().encode(req)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                print("Send json: \(jsonString)")
                
                return Network.shared
                    .request(target: .uploadLocation(image: photo, r: req))
                    .asObservable()
                    .flatMap({ (response) -> Observable<Void> in
                        Observable.just(())
                    })
                    .catchErrorJustReturn(())
                    .trackActivity(saving)
        }
        
        self.photoSent = photoSent
        
        let routeSettings = Observable.combineLatest(routeStartPoint, routeEndPoint) { (s: $0, e: $1) }
        
        let routeDataResponse = getRoute
            .withLatestFrom(routeSettings)
            .flatMap({ data -> Observable<GetRouteRequest> in
                let (s, e) = data
                
                let p1 = BoundingBoxPoint(lat: s.latitude, lon: s.longitude)
                let p2 = BoundingBoxPoint(lat: e.latitude, lon: e.longitude)
                
                let p = GetRouteRequest(from: GetRouteRequestPoint(coordinates: p1), to: GetRouteRequestPoint(coordinates: p2))
                
                let jsonData = try! JSONEncoder().encode(p)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                print(jsonString)
                
                return Observable.just(p)
            })
            .flatMap { s -> Observable<GetRouteResponse> in
                Network.shared
                    .request(target: .getRoute(r: s))
                    .mapObject(GetRouteResponse.self)
                    .asObservable()
                    .catchErrorJustReturn(GetRouteResponse())
//                    .flatMap({ (response) -> Observable<[RoutePoint]> in
//                        Observable.just(response.points)
//                    })
                    .trackActivity(saving)
        }
        
        self.routeData = routeDataResponse
        
        self.locations = locations
    }
    
}
