//
//  RadarViewController+ImagePicker.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit

extension RadarViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }

        // print out the image size as a test
        //print(image.size)
        capturedPhoto = image
        photoPreview.image = image
        photoFormView.doneButton.isEnabled = true
        state = .capturePhotoCategory
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerController?.dismiss(animated: true, completion: nil)
        state = .idle
    }
}

extension RadarViewController: PhotoFormViewDelegate {
    
    func didSelectCapturedPhotoCategory(_ category: String) {
        
        state = .choosePhotoLocation
        
        if let photo = capturedPhoto, let data = photo.jpeg(UIImage.JPEGQuality.medium) {
            viewModel.category.onNext(category)
            viewModel.capturedPhoto.onNext(data)
        }
        
        let bottomOffset = view.safeAreaInsets.bottom + 30
        
        let r = CGRect(x: 21, y: view.frame.size.height - bottomOffset - 123, width: 86, height: 123)
        
        UIView.animate(withDuration: 0.5) {
            self.photoPreview.frame = r
            self.photoPreview.layer.cornerRadius = 15
        }
    }
    
    func didCancelSelectingCategory() {
        state = .idle
        photoPreview.image = nil
        capturedPhoto = nil
    }
}
