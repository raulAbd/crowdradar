//
//  Helper.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation

class Helper {
    
    static let shared = Helper()
    
    let userID: String
    let debug = true
    
    init() {
        if let uid = UserDefaults.standard.string(forKey: "AppUserID") {
            userID = uid
        } else {
            userID = UUID().uuidString
            UserDefaults.standard.set(userID, forKey: "AppUserID")
        }
    }
    
}
