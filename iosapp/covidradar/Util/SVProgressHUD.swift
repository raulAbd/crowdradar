//
//  SVProgressHUD.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation
import RxSwift
import SVProgressHUD

extension SVProgressHUD {
    
    /**
     Bindable sink for MBProgressHUD show/hide methods.
     */
    static var rx_svprogresshud_animating: AnyObserver<Bool> {
        return AnyObserver { event in
            MainScheduler.ensureExecutingOnScheduler()
            switch (event) {
            case .next(let value):
                UIApplication.shared.isNetworkActivityIndicatorVisible = value
                if value {
                    SVProgressHUD.show()
                } else {
                    SVProgressHUD.dismiss()
                }
            case .error(let error):
                let error = "Binding error to UI: \(error)"
                print(error)
            case .completed:
                break
            }
        }
    }
}
