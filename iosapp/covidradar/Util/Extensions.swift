//
//  Extensions.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import Foundation

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
