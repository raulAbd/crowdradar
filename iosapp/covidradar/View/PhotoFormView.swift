//
//  PhotoFormView.swift
//  covidradar
//
//  Created by Alex Mavrichev on 26.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit

protocol PhotoFormViewDelegate: UIViewController {
    func didSelectCapturedPhotoCategory(_ category: String)
    func didCancelSelectingCategory()
}

final class PhotoFormView: UIView, UITextFieldDelegate {
    
    weak var delegate: PhotoFormViewDelegate?
    
    final private var categories: [String] = [
        "Queue",
        "Public gathering",
        "Heavy pedestrian traffic"
    ]
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var textFieldOutlet: CustomTextField!
    
    
    @IBAction func cancelAction(_ sender: Any) {
        endEditing(true)
        delegate?.didCancelSelectingCategory()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        (sender as! UIButton).isEnabled = false
        
        endEditing(true)
        
        if let txt = textFieldOutlet.text, txt != "" {
            delegate?.didSelectCapturedPhotoCategory(txt)
            textFieldOutlet.text = nil
        } else {
            delegate?.didSelectCapturedPhotoCategory(categories[pickerView.selectedRow(inComponent: 0)])
        }
    }
    
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 25.0
    private var fillColor: UIColor = .white // the color applied to the shadowLayer, rather than the view's backgroundColor
     
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.traitCollection.userInterfaceStyle == .dark {
            // User Interface is Dark
            fillColor = .black
        } else {
            // User Interface is Light
            fillColor = .white
        }
        
        backgroundColor = .clear
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
          
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor

            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 4.0)
            shadowLayer.shadowOpacity = 0.25
            shadowLayer.shadowRadius = 4

            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
        return false
    }
    
}

extension PhotoFormView: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
}
