//
//  UIView+Shadow.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit
extension UIView {
    func dropShadow(cornerRadius: CGFloat) {
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0, height: 4.0)
        shadowLayer.shadowOpacity = 1
        shadowLayer.shadowRadius = 4

        layer.insertSublayer(shadowLayer, at: 0)
    }
}
