//
//  TitleView.swift
//  covidradar
//
//  Created by Alex Mavrichev on 18.04.2020.
//  Copyright © 2020 Aleksandr Mavrichev. All rights reserved.
//

import UIKit

final class TitleView: UIView {
    
    private var gradientLayer: CAGradientLayer!
    
    lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Shade areas on the map where you saw a large crowd of people"
        l.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        l.textColor = .black
        l.numberOfLines = 0
        l.textAlignment = .center
        
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        
        isUserInteractionEnabled = false
        
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -80).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 80).isActive = true
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 57.52).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -200).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if gradientLayer == nil {
            gradientLayer = CAGradientLayer()
            
            gradientLayer.colors = [UIColor.white.cgColor, UIColor.clear.cgColor]
            gradientLayer.locations = [0.0 , 1.0]
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            gradientLayer.frame = bounds
            
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.frame = bounds
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
