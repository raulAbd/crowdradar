let config = require("./config").get();
let superagent = require("superagent");
let responses = require("./responses");
let location_util = require("./locations_util");
let format = require("./format");

module.exports.getPlace = event => {
  return new Promise(function(resolve, reject) {
    let link =
      "https://maps.googleapis.com/maps/api/geocode/json" +
      `?address=${JSON.parse(event.body).placeName}` +
      `&key=${config["google_api_key"]}`;

    superagent.get(link).end((err, response) => {
      if (err) console.log(err);
      console.log(response);
      let place_coordinates =
        response.body["results"][0]["geometry"]["location"]["lat"] +
        "," +
        response.body["results"][0]["geometry"]["location"]["lng"];
      resolve(responses.responseInfo(place_coordinates));
    });
  });
};

module.exports.getRoute = event => {
  return new Promise(function(resolve, reject) {
    let body = event.body;
    let origin = body.from;
    let destination = body.to;
    bbox = createBoundingBox(origin, destination);
    location_util.getClusters(bbox).then(clusterResponse => {
      let areas = clustersToAreas(JSON.parse(clusterResponse.body).clusters);
      let link =
        "https://route.ls.hereapi.com/routing/7.2/calculateroute.json" +
        `?apiKey=${config["here_api_key"]}` +
        `&waypoint0=geo!${origin.coordinates.lat},${origin.coordinates.lon}` +
        `&waypoint1=geo!${destination.coordinates.lat},${destination.coordinates.lon}` +
        "&mode=fastest;pedestrian;traffic:disabled" +
        `&avoidareas=${areas}` +
        "&routeAttributes=shape";
      superagent.get(link).end((err, response) => {
        // if (err) console.log(err);
        if (!err) {
          let coordinates = response.body.response.route[0].shape;
          //let link = createLink(coordinates);
          resolve(
            responses.responseRoute(format.herePointsToCoordinates(coordinates))
          );
        }
      });
    });
  });
};

function clustersToAreas(clusters) {
  areas = [];
  coef = 0.0000089;
  for (let cluster of clusters) {
    let lat = cluster[0];
    let lon = cluster[1];
    let radius = cluster[2];
    let topLeft = [
      lat + coef*radius,
      lon - coef*radius
    ];
    let bottomRight = [
      lat - coef * radius,
      lon + coef * radius
    ];
    areas.push([topLeft.join(), bottomRight.join()].join(";"));
    if (areas.length > 7) break;
  }
  return areas.join("!");
}

function createBoundingBox(origin, destination) {
  return {
    coordinates_top_right: {
      lat: 90,
      lon: 180
    },
    coordinates_bottom_left: {
      lat: -90,
      lon: -180
    }
  };
}
