let MongoClient = require("mongodb").MongoClient;
let responses = require("./responses");
let config = require("./config").get()

module.exports.addMarker = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          try {
            let db = client.db(config["db_name"]);
            addMarker(db, JSON.parse(event.body), function() {
              client.close();
            }).then(data => resolve(responses.responseSuccess));
          } catch (err) {
            console.log(err);
            resolve(responses.responseFailure(err));
          }
        }
      }
    );
  });
};

module.exports.getMarkers = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          selectMarkers(db, function() {
            client.close();
          }).then(arr => resolve(responses.responseMarkers(arr)));
        }
      }
    );
  });
};

function addMarker(db, body, callback) {
  return new Promise(function(resolve, reject) {
    let marker = {
      comment: body.comment,
      location: {
        type: "Point",
        coordinates: [body.point.lat, body.point.lng]
      }
    };
    db.createCollection("markers", function(err, results) {
      db.collection("markers").insertOne(marker, function(err, r) {
        callback();
        resolve(true);
      });
    });
  });
}

function selectMarkers(db, callback) {
  return new Promise(function(resolve, reject) {
    db.collection("markers")
      .find()
      .toArray((err, markerArr) => {
        if (err) console.log(err);
        callback();
        resolve(markerArr);
      });
  });
}
