let mongodb = require("mongodb");
module.exports.jsonMultipart = body => {
  return JSON.parse(
    JSON.parse(JSON.stringify(body)).description.replace(/\r?\n|\r/g, "")
  );
};

module.exports.coordinatesToArr = coordinates => {
  return [coordinates.lat, coordinates.lon];
};

module.exports.locationsArray = pointArr => {
  if (pointArr == undefined || pointArr.length == 0) return [];
  let points = [];
  for (let point of pointArr) {
    points.push({
      coordinates: {
        lat: point.location.coordinates[0],
        lon: point.location.coordinates[1]
      },
      category: point.category,
      radius: point.radius,
      id: point._id,
      datetime: new Date(point.received_requests[0].timestamp)
        .toISOString()
        .replace(/T/, " ")
        .replace(/\..+/, "")
        .replace(/-/g, ".")
    });
  }
  return points;
};

module.exports.unverifiedLocations = pointArr => {
  if (pointArr == undefined || pointArr.length == 0) return [];
  let points = [];
  for (let point of pointArr) {
    points.push({
      coordinates: {
        lat: point.location.coordinates[0],
        lon: point.location.coordinates[1]
      },
      category: point.category,
      radius: point.radius,
      id: point._id,
      ai_passed: point.ai_passed,
      photo: formatToFile(point.fileType, point.fileBase64),
      datetime: new Date(point.received_requests[0].timestamp)
        .toISOString()
        .replace(/T/, " ")
        .replace(/\..+/, "")
        .replace(/-/g, ".")
    });
  }
  return points;
};

module.exports.photos = pointArr => {
  if (pointArr == undefined || pointArr.length == 0) return [];
  let points = [];
  for (let point of pointArr) {
    points.push({
      id: point._id,
      photo: formatToFile(point.fileType, point.fileBase64)
    });
  }
  return points;
};

module.exports.pointsToCoordinates = points => {
  coords = [];
  for (let point of points) {
    coords.push([point.coordinates.lat, point.coordinates.lon, point.radius]);
  }
  return coords;
};

module.exports.herePointsToCoordinates = points => {
  let res = [];
  for (let point of points) {
    let coords = point.split(",");
    res.push({
      coordinates: {
        lat: parseFloat(coords[0]),
        lon: parseFloat(coords[1])
      }
    });
  }
  return res;
};

module.exports.toObjectIds = ids => {
  try {
    let res = [];
    for (let id of ids) {
      res.push(new mongodb.ObjectID(id));
    }
    return res;
  } catch (e) {
    return ids;
  }
};

module.exports.toObjectId = id => {
  try {
    return new mongodb.ObjectID(id);
  } catch (e) {
    return id;
  }
};

module.exports.photo = doc => {
  if (doc) {
    return {
      photo: {
        base64: doc.fileBase64,
        type: doc.fileType
      }
    };
  } else {
    return {
      photo: {
        base64: "",
        type: ""
      }
    };
  }
};

function formatToFile(type, base64) {
  let res = `data:${type};base64,${base64}`;
  return res;
}
