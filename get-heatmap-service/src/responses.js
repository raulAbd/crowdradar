let corsHeaders = {
  "Access-Control-Allow-Origin": "*",

  "Access-Control-Allow-Credentials": true
};

module.exports.responseSuccess = {
  message: "Request successful"
};

module.exports.responseFailure = errText => {
  return {
    message: "Something went wrong" + errText
  };
};

module.exports.responseLocations = locationsArray => {
  return {
    locations: locationsArray
  };
};

module.exports.responsePhotos = photosArray => {
  return {
    location_photos: photosArray
  };
};

module.exports.responsePhoto = photo => {
  return photo
};

module.exports.responseClusters = clustersArray => {
  return {
    statusCode: 200,
    headers: corsHeaders,
    body: JSON.stringify(
      {
        clusters: clustersArray
      },
      null,
      2
    )
  };
};

module.exports.responseInfo = obj => {
  return {
    statusCode: 200,
    headers: corsHeaders,
    body: JSON.stringify(
      {
        info: obj
      },
      null,
      2
    )
  };
};

module.exports.responseRoute = pointsArr => {
  return {
    points: pointsArr
  };
};

module.exports.responseMarkers = markerArray => {
  return {
    statusCode: 200,
    headers: corsHeaders,
    body: JSON.stringify(
      {
        markers: markerArray
      },
      null,
      2
    )
  };
};
