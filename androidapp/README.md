To run this project you need:

1. Obtain HereSDK(Premium Edition) license and past to AndroidManifest.xml, like this:

    <meta-data
            android:name="com.here.android.maps.appid"
            android:value="APP_ID" />
    <meta-data
            android:name="com.here.android.maps.apptoken"
            android:value="APP_TOKEN" />
    <meta-data
            android:name="com.here.android.maps.license.key"
            android:value="HERE_SDK_LICENSE" />

2. Download HereSDK(Premium edition) and place HERE-sdk.aar under app/libs directory
3. Build project
