package ru.svntech.radar.core

import io.reactivex.Completable
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import ru.svntech.radar.data.GetLocationsRequest
import ru.svntech.radar.data.GetLocationsResponse
import ru.svntech.radar.data.GetSafeRouteRequest
import ru.svntech.radar.data.SafeRouteResponse

interface ApiService {
    @Multipart
    @POST("new_location")
    fun createLocation(
        @Part file: MultipartBody.Part,
        @Part("description") json: RequestBody
    ): Completable

    @Headers("Content-Type: application/json")
    @POST("get_locations")
    fun getLocations(@Body request: GetLocationsRequest): Observable<GetLocationsResponse>

    @Headers("Content-Type: application/json")
    @POST("get_safe_route")
    fun getSafeRoute(@Body request: GetSafeRouteRequest): Observable<SafeRouteResponse>
}