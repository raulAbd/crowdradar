package ru.svntech.radar.di.core

import dagger.Component
import ru.svntech.radar.core.ApiService
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class
    ]
)
interface CoreComponent {
    val apiService: ApiService
}