package ru.svntech.radar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import ru.svntech.radar.presentation.map.MapContainerFragment
import ru.svntech.radar.util.PermissionHelper

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showRetrySnackbar(message: String, action: () -> Unit) {
        Snackbar.make(findViewById(R.id.coordinator), message, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry) { action() }
            show()
        }
    }

    fun showSnackbar(message: String, length: Int) {
        Snackbar.make(findViewById(R.id.coordinator), message, length).apply {
            show()
        }
    }
}
