package ru.svntech.radar.presentation.image

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_mark_image.*
import kotlinx.android.synthetic.main.item_chooser.view.*
import ru.svntech.radar.R
import ru.svntech.radar.RadarApplication
import ru.svntech.radar.di.app.AppModule
import ru.svntech.radar.di.app.DaggerAppComponent
import ru.svntech.radar.presentation.map.MapViewModel
import ru.svntech.radar.presentation.map.MapViewModelFactory
import javax.inject.Inject


val chooserItems = listOf(
    "Queue",
    "Public gathering",
    "Heavy pedestrian traffic"
)
const val ARG_IMAGE_URI = "imageURI"

class MarkImageFragment : DialogFragment() {
    private var selectedCategory: String = chooserItems[0]

    @Inject
    lateinit var mapViewModelFactory: MapViewModelFactory
    private val viewModel: MapViewModel by activityViewModels {
        mapViewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_mark_image, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        DaggerAppComponent
            .builder()
            .coreComponent(RadarApplication.coreComponent(requireContext()))
            .appModule(AppModule())
            .build()
            .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(categoryChooser) {
            adapter = CategoryChooserAdapter {
                selectedCategory = it
            }

            layoutManager = LinearLayoutManager(requireContext())
            itemAnimator = null

            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }

        btnSelectCategory.setOnClickListener {
            var category: String? = null

            val editTextValue = etCategory.text.trim()

            category = if (editTextValue.isNotEmpty()) {
                editTextValue.toString()
            } else {
                selectedCategory
            }

            viewModel.onCategorySelected(category)
            findNavController().popBackStack()
        }

        btnCancelSelection.setOnClickListener {
            findNavController().popBackStack()
        }

        arguments?.getString(ARG_IMAGE_URI)?.let {
            Glide.with(requireContext()).load(Uri.parse(it)).into(imagePreview)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = MarkImageFragment()
    }
}

class CategoryChooserAdapter(private val clickListener: (String) -> Unit) :
    RecyclerView.Adapter<CategoryChooserAdapter.CategoryChooserViewHolder>() {
    var clickedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryChooserViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_chooser,
            parent,
            false
        )
    )

    override fun getItemCount() = chooserItems.size

    override fun onBindViewHolder(holder: CategoryChooserViewHolder, position: Int) = holder.bind(
        chooserItems[position],
        clickListener
    )

    inner class CategoryChooserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(text: String, clickListener: (String) -> Unit) {
            itemView.txtChooser.text = text
            if (clickedPosition == adapterPosition) {
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.gray))
            } else {
                itemView.setBackgroundColor(Color.TRANSPARENT)
            }
            itemView.setOnClickListener {
                clickListener(text)
                notifyItemChanged(clickedPosition)
                clickedPosition = adapterPosition
                notifyItemChanged(clickedPosition)
            }
        }
    }
}
