package ru.svntech.radar.presentation.map

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.svntech.radar.core.ApiService

class MapViewModelFactory(val apiService: ApiService) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MapViewModel(
        apiService
    ) as T

}