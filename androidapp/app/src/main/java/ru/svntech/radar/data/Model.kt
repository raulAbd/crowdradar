package ru.svntech.radar.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UploadLocationRequest(
    val coordinates: BoundingBoxPoint,
    val category: String
)

data class BoundingBoxPoint(
    val lat: Double,
    val lon: Double
) : Serializable

data class GetLocationsRequest(
    @SerializedName("bounding_box")
    val boundingBoxPoint: GetLocationsBoundingBox
)

data class GetLocationsBoundingBox(
    @SerializedName("coordinates_top_right")
    val coordinatesTopRight: BoundingBoxPoint,
    @SerializedName("coordinates_bottom_left")
    val coordinatesBottomLeft: BoundingBoxPoint
)

data class GetLocationsResponse(
    val locations: List<MarkerLocation>
)

data class MarkerLocation(
    val id: String,
    val category: String,
    val radius: Double,
    val coordinates: BoundingBoxPoint,
    val datetime: String
) : Serializable

data class CoordinatesRequest(
    val coordinates: BoundingBoxPoint
)

data class GetSafeRouteRequest(
    val from: CoordinatesRequest,
    val to: CoordinatesRequest
)

data class Point(
    val coordinates: BoundingBoxPoint
)

data class SafeRouteResponse(
    val points: List<Point>
)