package ru.svntech.radar.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@Throws(IOException::class)
fun createImageFile(context: Context): File? {
    // Create an image file name
    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(
        "JPEG_${timeStamp}_", /* prefix */
        ".jpg", /* suffix */
        storageDir /* directory */
    )
}


fun getCompressedFile(file: File): File? {
    val compressionRatio = 2

    try {
        val bitmap = BitmapFactory.decodeFile(file.path)
        bitmap.compress(Bitmap.CompressFormat.JPEG, compressionRatio, FileOutputStream(file))
    } catch (t: Throwable) {
        t.printStackTrace()
    }

    return file
}