package ru.svntech.radar.di.app

import dagger.Component
import ru.svntech.radar.di.core.AppScope
import ru.svntech.radar.di.core.CoreComponent
import ru.svntech.radar.presentation.image.MarkImageFragment
import ru.svntech.radar.presentation.map.MapContainerFragment

@AppScope
@Component(
    dependencies = [CoreComponent::class],
    modules = [AppModule::class]
)
interface AppComponent {
    fun inject(mapContainerFragment: MapContainerFragment)
    fun inject(markImageFragment: MarkImageFragment)
}