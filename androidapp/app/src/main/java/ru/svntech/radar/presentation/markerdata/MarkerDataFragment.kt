package ru.svntech.radar.presentation.markerdata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_marker_data.*
import ru.svntech.radar.API_URL
import ru.svntech.radar.R
import ru.svntech.radar.data.MarkerLocation

const val ARG_DATA = "marker_data"

class MarkerDataFragment : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_marker_data, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (arguments?.getSerializable(ARG_DATA) as? MarkerLocation)?.let {
            txtCategory.text = it.category
            txtDate.text = it.datetime
            Glide.with(this)
                .load(API_URL + "/get_photo?id=${it.id}")
                .into(markerImage)
        }
    }
}