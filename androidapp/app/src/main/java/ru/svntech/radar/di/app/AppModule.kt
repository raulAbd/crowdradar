package ru.svntech.radar.di.app

import dagger.Module
import dagger.Provides
import ru.svntech.radar.core.ApiService
import ru.svntech.radar.presentation.map.MapViewModelFactory

@Module
class AppModule {
    @Provides
    fun provideMapViewModelFactory(apiService: ApiService): MapViewModelFactory =
        MapViewModelFactory(apiService)
}