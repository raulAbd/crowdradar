package ru.svntech.radar.presentation.map

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.here.android.mpa.common.GeoBoundingBox
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.routing.*
import io.reactivex.Notification
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import ru.svntech.radar.core.ApiService
import ru.svntech.radar.data.*
import ru.svntech.radar.util.getCompressedFile
import java.io.File
import java.util.concurrent.TimeUnit


class MapViewModel(private val apiService: ApiService) : ViewModel() {
    private val viewState = MutableLiveData<ViewState>()
    private val disposables = CompositeDisposable()
    private val mapMoveSubject: PublishSubject<GeoBoundingBox> =
        PublishSubject.create()

    init {
        listenMapMove()
    }

    fun observeStates() = viewState
    fun getMapMoveSubject() = mapMoveSubject

    fun onSendClick(category: String, file: File, coordinate: GeoCoordinate) {
        val compressedFile = getCompressedFile(file)

        if (compressedFile != null) {
            val body: MultipartBody.Part =
                MultipartBody.Part.createFormData(
                    "file",
                    "image.jpg",
                    compressedFile.asRequestBody("image/jpeg".toMediaTypeOrNull())
                )

            val requestJson = Gson().toJson(
                UploadLocationRequest(
                    coordinates = BoundingBoxPoint(
                        lat = coordinate.latitude,
                        lon = coordinate.longitude
                    ),
                    category = category
                )
            )

            val description: RequestBody =
                requestJson.toRequestBody("application/json".toMediaTypeOrNull())

            viewState.value = ViewState.Progress
            disposables.add(
                apiService.createLocation(body, description)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        viewState.value = ViewState.Init
                        viewState.value = ViewState.SuccessImageUpload
                    }, {
                        it.printStackTrace()
                        viewState.value = ViewState.NetworkError
                    })
            )
        } else {
            viewState.value = ViewState.NetworkError
        }
    }

    fun onCategorySelected(category: String) {
        viewState.value = ViewState.Image(category)
    }

    fun onCancelClick() {
        disposables.clear()
        viewState.value = ViewState.Init
    }

    fun onRoute(from: GeoCoordinate, to: GeoCoordinate) {
        viewState.value = ViewState.Progress
        disposables.add(
            apiService.getSafeRoute(
                GetSafeRouteRequest(
                    from = CoordinatesRequest(
                        coordinates = BoundingBoxPoint(
                            lat = from.latitude,
                            lon = from.longitude
                        )
                    ),
                    to = CoordinatesRequest(
                        coordinates = BoundingBoxPoint(
                            lat = to.latitude,
                            lon = to.longitude
                        )
                    )
                )
            ).flatMapSingle { calculateRoute(it.points) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it[0].route != null) {
                        viewState.value = ViewState.Route(it[0])
                    } else {
                        viewState.value = ViewState.RouteError
                    }
                }, {
                    it.printStackTrace()
                    viewState.value = ViewState.NetworkError
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    private fun listenMapMove() {
        disposables.add(
            mapMoveSubject
                .debounce(1, TimeUnit.SECONDS)
                //.doOnNext { viewState.value = ViewState.MapProgress }
                .map {
                    GetLocationsRequest(
                        GetLocationsBoundingBox(
                            coordinatesTopRight = BoundingBoxPoint(
                                lat = it.topLeft.latitude,
                                lon = it.bottomRight.longitude
                            ),
                            coordinatesBottomLeft = BoundingBoxPoint(
                                lat = it.bottomRight.latitude,
                                lon = it.topLeft.longitude
                            )
                        )
                    )
                }
                .switchMap { request ->
                    apiService.getLocations(request)
                        .map { Notification.createOnNext(it) }
                        .onErrorResumeNext { throwable: Throwable ->
                            Observable.just(
                                Notification.createOnError(throwable)
                            )
                        }.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                }
                .subscribe({
                    if (it.isOnError) {
                        viewState.value = ViewState.NetworkError
                    } else {
                        it.value?.locations?.let { locations ->
                            viewState.value = ViewState.Markers(locations)
                        }
                    }
                }, Throwable::printStackTrace)
        )
    }

    fun calculateRoute(points: List<Point>): Single<List<RouteResult>> =
        Single.create { emiter ->
            val coreRouter = CoreRouter()

            val routeOptions = RouteOptions().apply {
                transportMode = RouteOptions.TransportMode.PEDESTRIAN
                routeType = RouteOptions.Type.SHORTEST
                routeCount = 1
            }
            coreRouter.calculateRoute(points.map {
                GeoCoordinate(
                    it.coordinates.lat,
                    it.coordinates.lon
                )
            }, routeOptions, object : Router.Listener<List<RouteResult>, RoutingError> {
                override fun onCalculateRouteFinished(
                    list: List<RouteResult>?,
                    routingError: RoutingError
                ) {
                    if (routingError == RoutingError.NONE) {
                        list?.let {
                            emiter.onSuccess(it)
                        } ?: emiter.onError(Throwable("Routing error"))
                    } else {
                        emiter.onError(Throwable("Routing error"))
                    }
                }

                override fun onProgress(p0: Int) {}
            })
        }

    fun onRouteSelectionClicked() {
        viewState.value = ViewState.RouteSelection
    }

    fun cancelRouteClicked() {
        viewState.value = ViewState.Init
    }
}
