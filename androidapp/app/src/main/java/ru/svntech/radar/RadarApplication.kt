package ru.svntech.radar

import android.app.Application
import android.content.Context
import ru.svntech.radar.di.core.CoreComponent
import ru.svntech.radar.di.core.DaggerCoreComponent
import ru.svntech.radar.di.core.NetworkModule

class RadarApplication : Application() {
    lateinit var coreComponent: CoreComponent

    companion object {
        @JvmStatic
        fun coreComponent(context: Context) =
            (context.applicationContext as? RadarApplication)?.coreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initCoreDependencyInjection()
    }

    private fun initCoreDependencyInjection() {
        coreComponent = DaggerCoreComponent
            .builder()
            .networkModule(NetworkModule())
            .build()
    }
}