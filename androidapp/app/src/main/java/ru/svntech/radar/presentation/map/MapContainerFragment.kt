package ru.svntech.radar.presentation.map

import android.app.Activity
import android.content.ClipData
import android.content.Intent
import android.graphics.Color
import android.graphics.PointF
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.here.android.mpa.common.*
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.*
import kotlinx.android.synthetic.main.fragment_map_container.*
import ru.svntech.radar.MainActivity
import ru.svntech.radar.R
import ru.svntech.radar.RadarApplication
import ru.svntech.radar.data.MarkerLocation
import ru.svntech.radar.di.app.AppModule
import ru.svntech.radar.di.app.DaggerAppComponent
import ru.svntech.radar.presentation.image.ARG_IMAGE_URI
import ru.svntech.radar.presentation.markerdata.ARG_DATA
import ru.svntech.radar.util.PermissionHelper
import ru.svntech.radar.util.TAG
import ru.svntech.radar.util.createImageFile
import ru.svntech.radar.util.safeLet
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference
import javax.inject.Inject

const val MAP_ZOOM_LEVEL = 14.0
const val REQUEST_CAMERA_IMAGE = 1

sealed class ViewState {
    object Init : ViewState()
    data class Image(val category: String) : ViewState()
    data class Markers(val locations: List<MarkerLocation>) : ViewState()
    data class Route(val routeResult: RouteResult) : ViewState()
    object SuccessImageUpload : ViewState()
    object NetworkError : ViewState()
    object Progress : ViewState()
    object MapProgress : ViewState()
    object ErrorPermissions : ViewState()
    object RouteError : ViewState()
    object RouteSelection : ViewState()
}

class MapContainerFragment : Fragment(), PositioningManager.OnPositionChangedListener,
    PermissionHelper.ResultListener {

    private var lightBlueColor: Int = Color.BLUE
    private var currentRoute: MapRoute? = null
    private var destinationMarker: MapMarker? = null

    @Inject
    lateinit var mapViewModelFactory: MapViewModelFactory
    private val viewModel: MapViewModel by activityViewModels {
        mapViewModelFactory
    }

    private var photoFile: File? = null
    private var imageURI: Uri? = null
    private var userPositioned: Boolean = false
    private lateinit var permissionHelper: PermissionHelper
    private lateinit var mapFragment: AndroidXMapFragment
    private var positioningManager: PositioningManager? = null
    private var map: Map? = null
    private var userCoordinates: GeoCoordinate? = null
    private var mapMarkers: HashMap<MapMarker, MarkerLocation> = HashMap()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_map_container, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        DaggerAppComponent
            .builder()
            .coreComponent(RadarApplication.coreComponent(requireContext()))
            .appModule(AppModule())
            .build()
            .inject(this)

        fabFindMe.setOnClickListener { showUserOnMap() }
        btnCapture.setOnClickListener { sendCaptureIntent() }
        fabCancel.setOnClickListener { viewModel.onCancelClick() }
        fabRoute.setOnClickListener { viewModel.onRouteSelectionClicked() }
        btnDone.setOnClickListener {
            safeLet(userCoordinates, map?.center) { from, to ->
                viewModel.onRoute(from, to)
            }
        }
        btnCancelRoute.setOnClickListener { viewModel.cancelRouteClicked() }

        render(ViewState.Init)

        viewModel.observeStates().observe(viewLifecycleOwner, Observer { state ->
            render(state)
        })

        handleAndroidPermissions()

        lightBlueColor = ContextCompat.getColor(requireContext(), R.color.light_blue)
    }

    fun render(viewState: ViewState): Unit = when (viewState) {
        ViewState.Init -> {
            txtHeading.visibility = View.GONE
            routeActions.visibility = View.GONE
            mainActions.visibility = View.VISIBLE
            fabRoute.show()
            fabFindMe.show()
            centralMarker.visibility = View.GONE
            imageActions.visibility = View.GONE
            routeActions.visibility = View.GONE
            mapProgress.visibility = View.GONE
            gradient.visibility = View.GONE
            btnCancelRoute.visibility = View.GONE
            mapRemoveRoute()
        }
        is ViewState.Image -> {
            gradient.visibility = View.VISIBLE
            txtHeading.text = "Place a mark where you\nsaw this crowd"
            txtHeading.visibility = View.VISIBLE

            imageURI?.let {
                Glide.with(requireContext())
                    .load(it)
                    .apply(RequestOptions.bitmapTransform(RoundedCorners(20)))
                    .into(imageThumb)
            }
            btnSend.isEnabled = true
            btnSendIcon.visibility = View.VISIBLE
            btnSendText.visibility = View.VISIBLE
            mainActions.visibility = View.GONE
            imageActions.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            centralMarker.visibility = View.VISIBLE

            fabRoute.hide()

            btnSend.setOnClickListener {
                safeLet(photoFile, map) { _file, _map ->
                    viewModel.onSendClick(viewState.category, _file, _map.center)
                }
            }
            mapProgress.visibility = View.GONE
        }
        ViewState.NetworkError -> {
            mapProgress.visibility = View.GONE
            btnSend.isEnabled = true
            btnSendIcon.visibility = View.VISIBLE
            btnSendText.visibility = View.VISIBLE

            btnDone.isEnabled = true
            btnDoneProgress.visibility = View.GONE
            btnDoneImage.visibility = View.VISIBLE
            btnDoneText.visibility = View.VISIBLE

            progressBar.visibility = View.GONE
            showSnackbar("Network error")
        }
        ViewState.ErrorPermissions -> TODO()
        ViewState.Progress -> {
            btnSend.isEnabled = false
            mapProgress.visibility = View.GONE
            btnSendIcon.visibility = View.GONE
            btnSendText.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            btnDone.isEnabled = false
            btnDoneProgress.visibility = View.VISIBLE
            btnDoneImage.visibility = View.GONE
            btnDoneText.visibility = View.GONE
        }
        is ViewState.Markers -> showMarkers(viewState.locations)
        is ViewState.Route -> {
            showSnackbar("Safe route has been calculated", Snackbar.LENGTH_SHORT)

            fabFindMe.hide()
            gradient.visibility = View.GONE
            txtHeading.visibility = View.GONE
            routeActions.visibility = View.GONE
            mapProgress.visibility = View.GONE
            btnCancelRoute.visibility = View.VISIBLE
            centralMarker.visibility = View.GONE
            showRoute(viewState.routeResult)
        }
        ViewState.RouteError -> {
            showSnackbar("Route calculation error")
            btnDone.isEnabled = true
            btnDoneProgress.visibility = View.GONE
            btnDoneImage.visibility = View.VISIBLE
            btnDoneText.visibility = View.VISIBLE
        }
        ViewState.MapProgress -> {
            mapProgress.visibility = View.VISIBLE
        }
        ViewState.RouteSelection -> {
            btnDone.isEnabled = true
            btnDoneProgress.visibility = View.GONE
            btnDoneImage.visibility = View.VISIBLE
            btnDoneText.visibility = View.VISIBLE

            mainActions.visibility = View.GONE
            centralMarker.visibility = View.VISIBLE
            routeActions.visibility = View.VISIBLE
            gradient.visibility = View.VISIBLE
            txtHeading.text = "Select a destination\nby moving the map"
            txtHeading.visibility = View.VISIBLE
            fabRoute.hide()
        }
        ViewState.SuccessImageUpload -> showSnackbar(
            "Thank you for submission!",
            Snackbar.LENGTH_SHORT
        )
    }

    override fun onPositionFixChanged(
        locationMethod: PositioningManager.LocationMethod,
        locationStatus: PositioningManager.LocationStatus
    ) {
    }

    override fun onPositionUpdated(
        method: PositioningManager.LocationMethod?,
        geoPosition: GeoPosition?,
        isMapMatched: Boolean
    ) {
        geoPosition?.coordinate?.let {
            userCoordinates = it
        }

        if (!userPositioned) {
            showUserOnMap()
            userPositioned = true
        }
    }

    override fun onResume() {
        super.onResume()
        positioningManager?.start(PositioningManager.LocationMethod.GPS_NETWORK)

    }

    override fun onPause() {
        positioningManager?.stop()
        super.onPause()
    }

    override fun onDestroy() {
        positioningManager?.removeListener(this)
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CAMERA_IMAGE && resultCode == Activity.RESULT_OK) {
            imageURI?.let {
                findNavController().navigate(
                    R.id.action_mapContainerFragment_to_markImageFragment,
                    bundleOf(ARG_IMAGE_URI to imageURI.toString())
                )
            } ?: showSnackbar("Error occurred while requesting camera")
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionHelper.onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun permissionsGranted() {
        initMap()
    }

    override fun permissionsDenied() {
        (requireActivity() as MainActivity).showRetrySnackbar("To use the application you need permissions") {
            permissionHelper.request(this)
        }
    }

    private fun handleAndroidPermissions() {
        permissionHelper = PermissionHelper(this)
        permissionHelper.request(this)
    }

    private fun initMap() {
        mapFragment = AndroidXMapFragment().also {
            it.init(ApplicationContext(requireContext())) { error ->
                if (error == OnEngineInitListener.Error.NONE) {
                    it.positionIndicator?.apply {
                        isVisible = true
                    }

                    setupPositionManager()

                    userCoordinates = positioningManager?.lastKnownPosition?.coordinate
                    map = mapFragment.map
                    map?.zoomLevel = MAP_ZOOM_LEVEL

                    addMapGestureListener(mapFragment.mapGesture)
                } else {
                    Log.e(TAG(), "ERROR: Cannot initialize Map Fragment")
                }
            }
        }

        childFragmentManager.beginTransaction().replace(R.id.mapContainer, mapFragment).commit()
    }

    private fun addMapGestureListener(mapGesture: MapGesture?) {
        mapGesture?.apply {
            addOnGestureListener(object : MapGesture.OnGestureListener {
                override fun onLongPressRelease() {}

                override fun onRotateEvent(p0: Float): Boolean {
                    return false
                }

                override fun onMultiFingerManipulationStart() {
                }

                override fun onPinchLocked() {
                }

                override fun onPinchZoomEvent(p0: Float, p1: PointF): Boolean {
                    return false
                }

                override fun onTapEvent(p0: PointF): Boolean {
                    Log.d(TAG(), "TAP")
                    return true
                }

                override fun onPanStart() {

                }

                override fun onMultiFingerManipulationEnd() {

                }

                override fun onDoubleTapEvent(p0: PointF): Boolean {
                    return false
                }

                override fun onPanEnd() {
                    map?.boundingBox?.let {
                        viewModel.getMapMoveSubject().onNext(it)
                    }
                }

                override fun onTiltEvent(p0: Float): Boolean {
                    return false
                }

                override fun onMapObjectsSelected(objects: MutableList<ViewObject>): Boolean {
                    selectMarker(objects)
                    return false
                }

                override fun onRotateLocked() {
                }

                override fun onLongPressEvent(p0: PointF): Boolean {
                    return false
                }

                override fun onTwoFingerTapEvent(p0: PointF): Boolean {
                    return false
                }
            }, 0, false)
        }
    }

    private fun showUserOnMap() {
        safeLet(map, userCoordinates) { _map, _coordinates ->
            _map.setCenter(
                _coordinates,
                Map.Animation.NONE
            )
            _map.boundingBox?.let {
                viewModel.getMapMoveSubject().onNext(it)
            }
        }
    }

    private fun sendCaptureIntent() {
        photoFile = null
        imageURI = null
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            try {
                photoFile = createImageFile(requireContext())
                imageURI = photoFile?.let {
                    FileProvider.getUriForFile(
                        requireContext(),
                        getString(R.string.file_provider_authority),
                        it
                    )
                }
            } catch (ex: IOException) {
                Log.e("TakePicture", ex.message)
            }

            imageURI?.let {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, it)
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.clipData = ClipData.newRawUri("", it)
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA_IMAGE)
            }
        }
    }

    private fun setupPositionManager() {
        positioningManager = PositioningManager.getInstance().apply {
            addListener(WeakReference(this@MapContainerFragment))
            dataSource = LocationDataSourceGoogleServices.getInstance()
            start(PositioningManager.LocationMethod.GPS_NETWORK)
        }
    }


    private fun showMarkers(markerLocations: List<MarkerLocation>) {
        markerLocations.forEach {
            if (!mapMarkers.values.any { placedMarker -> it.id == placedMarker.id }) {
                addCircleObject(it)
                addMapMarkerObject(it)
            }
        }
    }

    private fun addCircleObject(markerLocation: MarkerLocation) {
        with(markerLocation) {
            map?.apply {
                val circle = MapCircle().apply {
                    fillColor = lightBlueColor
                    radius = markerLocation.radius
                    center = GeoCoordinate(coordinates.lat, coordinates.lon)
                }

                addMapObject(circle)
            }
        }
    }

    private fun addMapMarkerObject(markerLocation: MarkerLocation) = with(markerLocation) {
        map?.apply {
            val marker = MapMarker(GeoCoordinate(coordinates.lat, coordinates.lon)).apply {
                title = "Map marker"
                anchorPoint = PointF(icon.width.toFloat() / 2, icon.height.toFloat())
            }

            addMapObject(marker)
            mapMarkers[marker] = markerLocation
        }
    }

    private fun selectMarker(objects: MutableList<ViewObject>) {
        objects.forEach {
            if (it.baseType == ViewObject.Type.USER_OBJECT) {
                if ((it as MapObject).type == MapObject.Type.MARKER) {
                    mapMarkers[it]?.let {
                        findNavController().navigate(
                            R.id.action_mapContainerFragment_to_markerDataFragment,
                            Bundle().apply {
                                putSerializable(ARG_DATA, it)
                            })
                        return
                    }
                }
            }
        }
    }

    private fun mapRemoveRoute() {
        safeLet(map, currentRoute) { _map, route ->
            _map.removeMapObject(route)
            destinationMarker?.let { _map.removeMapObject(it) }
        }

        destinationMarker = null
        currentRoute = null
    }

    private fun showRoute(routeResult: RouteResult) {
        currentRoute = MapRoute(routeResult.route).apply {
            color = lightBlueColor
        }

        routeResult.route.destination?.let {
            val image = Image().apply {
                setImageResource(R.drawable.flag)
            }
            destinationMarker = MapMarker().setCoordinate(it)
                .setAnchorPoint(PointF(0F, image.height.toFloat()))
                .setIcon(image)
        }

        map?.apply {
            addMapObject(currentRoute!!)
            destinationMarker?.let { addMapObject(it) }
        }
    }

    private fun showSnackbar(message: String, length: Int = Snackbar.LENGTH_LONG) {
        (requireActivity() as MainActivity).showSnackbar(message, length)
    }
}

